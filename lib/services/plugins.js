/*!
 * renato-wiki is a fast, simple and modular wiki platform written in node.js.
 * Copyright (C) 2017  Ole Reglitzki
 *
 * This file is part of renato-wiki.
 *
 * renato-wiki is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * renato-wiki is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
"use strict";

const Promise = require("bluebird");

/*===================================================== Exports  =====================================================*/

exports.load = load;

/*==================================================== Functions  ====================================================*/

function load(module, renato) {
  let data;
  if (typeof module === "object" && module !== null && module.hasOwnProperty("module")) {
    data = module;
    module = module.module;
  } else {
    data = {module};
  }
  if (typeof module === "string") {
    try {
      module = require.main.require(module);
    } catch (e) {
      if (e.message !== "Cannot find module '" + module + "'") { throw e; }
      try {
        module = require(module);
      } catch (e) {
        if (e.message !== "Cannot find module '" + module + "'") { throw e; }
        throw new Error("Module not found: '" + module + "'");
      }
    }
  }
  if (typeof module === "function") {
    module = Promise
        .method(module)(renato, data)
        .then((result) => result === null ? module : result);
  }
  return Promise.resolve(module);
}
