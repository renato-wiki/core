/*!
 * renato-wiki is a fast, simple and modular wiki platform written in node.js.
 * Copyright (C) 2017  Ole Reglitzki
 *
 * This file is part of renato-wiki.
 *
 * renato-wiki is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * renato-wiki is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
"use strict";

/*===================================================== Exports  =====================================================*/

exports.index = resolveIndex;

/*==================================================== Functions  ====================================================*/

// todo cache a map uri:{index,forwardURI} (during index build) to vastly improve access performance

function resolveIndex(rootIndex, dataKey, path) {
  let index = rootIndex, forwarded = false;
  if (index == null) { return null; }
  if (!Array.isArray(path) || !path.length) { return {index, forwarded}; }
  let current, _last = path.length - 1, i;
  for (i = 0; i < _last; i++) {
    current = path[i];
    index = findFirst(index[dataKey].categoryMap, [current, current + ".c0"]);
    if (index == null) { return {index, forwarded}; }
    forwarded = forwarded || index.id !== current;
  }
  current = path[_last];
  index = findFirst(index[dataKey].categoryMap, [current, current + ".c0"]) ||
      findFirst(index[dataKey].pageMap, [current, current + ".p0"]);
  if (index == null) { return {index, forwarded}; }
  forwarded = forwarded || index.id !== current;
  return {index, forwarded};
}

function findFirst(map, ids) {
  let id;
  for (let i = 0; i < ids.length; i++) {
    id = ids[i];
    if (map.hasOwnProperty(id)) { return map[id]; }
  }
  return null;
}

