/*!
 * renato-wiki is a fast, simple and modular wiki platform written in node.js.
 * Copyright (C) 2017  Ole Reglitzki
 *
 * This file is part of renato-wiki.
 *
 * renato-wiki is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * renato-wiki is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
"use strict";

const _ = require("lodash");
const fs = require("fs");
const path = require("path");
const Promise = require("bluebird");

const DEFAULT_CONFIG = require("../constants/default-config");

const fsReadFile = Promise.promisify(fs.readFile);

/*===================================================== Exports  =====================================================*/

exports.prepare = prepareConfig;

/*==================================================== Functions  ====================================================*/

function prepareConfig(config, dir = null) {
  if (typeof config === "string") { return readConfigFile(config).then((content) => prepareConfig(content, config)); }
  if (config.variables == null) { config.variables = {}; }
  // if dir is provided, use as default base path
  if (dir != null) {
    if (config.paths == null) { config.paths = {}; }
    if (!config.paths.hasOwnProperty("base")) { config.paths.base = dir; }
  }
  // allow title to be a string
  if (typeof config.title === "string") { config.title = {template: config.title}; }
  // merge defaults
  _.defaultsDeep(config, DEFAULT_CONFIG);
  // make paths absolute
  let paths = config.paths;
  paths.content = path.resolve(paths.base, paths.content);
  paths.public = path.resolve(paths.base, paths.public);
  // all done
  return Promise.resolve(config);
}

function readConfigFile(dir) { return fsReadFile(path.join(dir, "config.json")).then(_.ary(JSON.parse, 1)); }
