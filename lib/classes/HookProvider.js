/*!
 * renato-wiki is a fast, simple and modular wiki platform written in node.js.
 * Copyright (C) 2017  Ole Reglitzki
 *
 * This file is part of renato-wiki.
 *
 * renato-wiki is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * renato-wiki is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
"use strict";

const Promise = require("bluebird");

/*===================================================== Exports  =====================================================*/

module.exports = HookProvider;

/*==================================================== Functions  ====================================================*/

function HookProvider() {
  this.handlers = {};
}

HookProvider.prototype.addHandler = function (key, handler, order = 0) {
  let entry = {fn: handler, order};
  if (this.handlers.hasOwnProperty(key)) {
    let handlers = this.handlers[key];
    let i = handlers.length;
    while (i > 0) {
      if (handlers[--i].order < order) {
        i++;
        break;
      }
    }
    handlers.splice(i, 0, entry);
  } else {
    this.handlers[key] = [entry];
  }
  return this;
};

HookProvider.prototype.hookAction = function (key, data, options) {
  if (this.handlers.hasOwnProperty(key)) {
    return Promise.map(this.handlers[key], (entry) => entry.fn(data), options);
  } else {
    return Promise.resolve();
  }
};

HookProvider.prototype.hookReduce = function (key, data) {
  if (this.handlers.hasOwnProperty(key)) {
    return Promise.reduce(this.handlers[key], (result, entry) => entry.fn(result, data), data);
  } else {
    return Promise.resolve(data);
  }
};
