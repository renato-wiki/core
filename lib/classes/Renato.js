/*!
 * renato-wiki is a fast, simple and modular wiki platform written in node.js.
 * Copyright (C) 2017  Ole Reglitzki
 *
 * This file is part of renato-wiki.
 *
 * renato-wiki is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * renato-wiki is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
"use strict";

const _ = require("lodash");
const Promise = require("bluebird");
const EventEmitter = require("events");
const stringTemplate = require("string-template");
const stringTemplateCompile = require("string-template/compile");

const api = require("../api/index");
const indexS = require("../services/index/index");
const configS = require("../services/config");
const plugins = require("../services/plugins");
const loggerS = require("../constants/logger");

const LOGGER = loggerS.APP;

const HookProvider = require("./HookProvider");
const APIData = require("./APIData");

/*===================================================== Exports  =====================================================*/

module.exports = Renato;

module.exports.INDEX_DATA_KEY = Symbol("Resolve hidden data within Renato#index.");

module.exports.format = {
  parse: stringTemplate,
  compile: stringTemplateCompile
};

/*==================================================== Functions  ====================================================*/

/*------------------------------------------------------ Renato ------------------------------------------------------*/

_.assign(Renato.prototype, HookProvider.prototype);
_.assign(Renato.prototype, EventEmitter.prototype);
function Renato(config) {
  if (this == null) { return new Renato(config); }
  EventEmitter.call(this);
  HookProvider.call(this);
  this.extensions = [];
  this.indexDataKey = Renato.INDEX_DATA_KEY;
  this.index = null;
  this.debug = process.env.NODE_ENV === "development";
  this.ready = configS
      .prepare(config, Renato.format)
      .then((config) => {
        this.config = config;
        this.apiData = new APIData(this);
        this.log = LOGGER.child({config: _.pick(config, ["paths", "plugins"])});
        this.log.verbose("preparing");
        return Promise
            .map(config.plugins, _.partial(plugins.load, _, this))
            .then((plugins) => this.plugins = plugins);
      })
      .then(() => this.hookAction("core::init"))
      .then(() => this.buildIndex())
      .then(() => {
        this.log.info("ready");
        return this.emit("ready"); // return prevents node.js warning about promise creation without return
      });

  this.addHandler("core::routes:bind", _.partial(api.bindRoutes, this));
}

Renato.shutdown = () => loggerS.shutdown(); // todo make logger instances to be shut-down separately

Renato.prototype.enableExtension = function (extension) {
  if (!_.includes(this.extensions, extension)) { this.extensions.push(extension); }
};

Renato.prototype.bindRoutes = function (uiRouter, apiRouter) {
  let routers = {ui: uiRouter, api: apiRouter};
  return this.ready
      .then(() => {
        let logMiddleware = this.log.middleware();
        let log = this.log.child({routers}); // todo define routers parser for logger
        log.debug("binding logger middleware");
        if (uiRouter != null) { uiRouter.use(logMiddleware); }
        if (apiRouter != null) { apiRouter.use(logMiddleware); }
        log.verbose("binding routes");
        return this
            .hookAction("core::routes:bind", routers, {concurrency: 1})
            .then(() => { this.log.info("binding routes complete"); });
      });
};

Renato.prototype.buildIndex = function () {
  this.emit("core::index:update:pre");
  return indexS
      .build(this.indexDataKey, this.config, this.extensions, this.handlers)
      .then((result) => this.hookReduce("core::index:update:prepare", result))
      .then((result) => {
        this.index = result;
        this.emit("core::index:update:post", this.index);
        return this.index;
      });
};

Renato.prototype.resolveIndex = function (path) { return indexS.resolve(this.index, this.indexDataKey, path); };
