/*!
 * renato-wiki is a fast, simple and modular wiki platform written in node.js.
 * Copyright (C) 2017  Ole Reglitzki
 *
 * This file is part of renato-wiki.
 *
 * renato-wiki is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * renato-wiki is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
"use strict";

const root = require("./routes/root");
const content = require("./routes/content");
const favicon = require("./routes/favicon");
const publ = require("./routes/public");

/*===================================================== Exports  =====================================================*/

exports.bindRoutes = bindRoutes;

/*==================================================== Functions  ====================================================*/

function bindRoutes(renato, routers) {
  let uiRouter = routers.ui, apiRouter = routers.api;
  if (uiRouter != null) {
    favicon.bindRoutes(renato, uiRouter);
    publ.bindRoutes(renato, uiRouter);
  }
  if (apiRouter != null) {
    root.bindRoutes(renato, apiRouter);
    content.bindRoutes(renato, apiRouter);
  }
}
