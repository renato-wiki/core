/*!
 * renato-wiki is a fast, simple and modular wiki platform written in node.js.
 * Copyright (C) 2017  Ole Reglitzki
 *
 * This file is part of renato-wiki.
 *
 * renato-wiki is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * renato-wiki is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
"use strict";

const _ = require("lodash");
const parseURI = require("../../services/parse-uri");

/*===================================================== Exports  =====================================================*/

exports.bindRoutes = bindRoutes;
exports.getData = getData;

/*==================================================== Functions  ====================================================*/

function bindRoutes(renato, router) {
  let fn = _.partial(routeContent, renato);
  router.get("/content", fn);
  router.get("/content/*", fn);
}

function routeContent(renato, req, res, next) {
  let data = renato.apiData.getContent(parseURI(req.params[0]));
  if (data == null) { return next(); }
  // todo if data.forwarded return res.redirect(303, ...)
  switch (data.index.type) {
    case "category":
      sendCategory(res, data);
      break;
    case "page":
      sendPage(renato, req, res, data);
      break;
    default:
      throw new Error("Index type '" + data.index.type + "' not supported.");
  }
}

function sendCategory(res, data) { res.json(data); }

function sendPage(renato, req, res, data) {
  let type = req.accepts(["json", "text", "html"]);
  let p = req.query.parsed !== "0";
  let showParsed = !!(p && (type === "html" || req.query.parsed));
  let ext = data.index[renato.indexDataKey].ext;
  let hook = "core::content:page:fetch:" + ext + ":" + (showParsed ? "parsed" : "plain");
  renato
      .hookReduce(hook, data.index)
      .then((content) => {
        switch (type) {
          case "text":
          case "html":
            if (!showParsed) { res.set("Content-Type", "text/plain"); }
            res.send(content);
            break;
          case "json":
            res.json(_.assign(data, {content, parsed: showParsed}));
            break;
          default:
            res.status(406).send("Not Acceptable");
        }
      });
}

function getData(renato, pathArr) {
  let data = renato.resolveIndex(_.compact(pathArr));
  return data != null && data.index != null ? data : null;
}
