/*!
 * renato-wiki is a fast, simple and modular wiki platform written in node.js.
 * Copyright (C) 2017  Ole Reglitzki
 *
 * This file is part of renato-wiki.
 *
 * renato-wiki is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * renato-wiki is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
"use strict";

/* eslint no-sync: off */

const fs = require("fs");
const path = require("path");
const oddlog = require("oddlog");
const Promise = require("bluebird");

const PREFIX = "renato/core/";
const DIR_LOGS = path.join(__dirname, "../../logs");

const APP = {
  shy: true,
  transports: [
    {type: "file-rotate", path: path.join(DIR_LOGS, "app.log"), size: "256KiB", maxFiles: 4, level: oddlog.TRACE},
    {type: "threshold", threshold: oddlog.WARN, transports: {type: "stream", level: oddlog.DEBUG}}
  ]
};
const REQ = {
  shy: true,
  transports: [
    {type: "file-rotate", path: path.join(DIR_LOGS, "req.log"), size: "256KiB", maxFiles: 2, level: oddlog.TRACE},
    {type: "threshold", threshold: oddlog.WARN, transports: {type: "stream", level: oddlog.DEBUG}}
  ]
};

/*================================================ Initial Execution  ================================================*/

if (!fs.existsSync(DIR_LOGS)) { fs.mkdirSync(DIR_LOGS); }

/*===================================================== Exports  =====================================================*/

exports.APP = oddlog(PREFIX + "app", APP);
exports.REQ = oddlog(PREFIX + "req", REQ);

exports.LIST = [exports.APP, exports.REQ];

exports.shutdown = () => Promise.promisify(oddlog.closeAll)(true, exports.LIST);
