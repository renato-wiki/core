/*!
 * renato-wiki is a fast, simple and modular wiki platform written in node.js.
 * Copyright (C) 2017  Ole Reglitzki
 *
 * This file is part of renato-wiki.
 *
 * renato-wiki is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * renato-wiki is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
"use strict";

const path = require("path");

const LINK = "<a href=\"https://gitlab.com/renato-wiki/core\">Powered by renato-wiki</a>";

/*===================================================== Exports  =====================================================*/

module.exports = {
  paths: {
    base: path.dirname(require.main.filename),
    content: "content",
    public: "public"
  },
  variables: {
    name: "renato-wiki"
  },
  sort: {
    page: ["meta.sort", "meta.title"],
    category: ["meta.sort", "meta.title"]
  },
  plugins: [
    {
      module: "@renato-wiki/theme-nerissa",
      title: {
        template: "{{view}} | {name}",
        custom: {
          home: "{name}",
          category: "{{index.meta.title}} ({{view}}) &middot; {name}",
          page: "{{index.meta.title}} &middot; {name}"
        }
      },
      branding: "{name}",
      footer: "Copyright &copy; {{year}} - " + LINK
    },
    "@renato-wiki/plugin-markdown"
  ]
};
