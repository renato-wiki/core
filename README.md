# renato-wiki/core

[![License](https://img.shields.io/badge/license-AGPL--3.0-blue.svg?style=flat-square)](LICENSE)
[![Version](https://img.shields.io/npm/v/@renato-wiki/core.svg?style=flat-square)](https://www.npmjs.com/package/@renato-wiki/core)
[![build status](https://gitlab.com/renato-wiki/core/badges/master/build.svg)](https://gitlab.com/renato-wiki/core/commits/master)

renato-wiki is a fast, simple and modular wiki platform written in node.js

See the [basic example project](https://gitlab.com/renato-wiki/example-basic) for setup.

## Status Quo

This is very early development, the wiki is read-only and may lack stability. Use at own risk.

<!-- TODO add screenshots, speed ranks, etc. -->
<!-- TODO create hosted live instance for demo -->

## Appendix

### Thanks

Many thanks to everyone who supports this project by usage, contribution and/or spreading the word!

All contributors may add themselves to the `contributors` field within the *package.json* regardless of the amount of
changes!

### Referral

If you wish to support the author of this project regarding costs, you may use
[this referral link](https://m.do.co/c/c15c9d5a2494) to create (and use) a [DigitalOcean](https://www.digitalocean.com/)
account. I can recommend their services for any kind of node.js projects; They run without problems and only short
downtime now and then (speaking of cheapest droplets). You will get a $10 credit for using the referral link and once
you've spent $25 with them, the author ([*frissdiegurke*](//gitlab.com/frissdiegurke)) will receive $25 for his droplets
as well. This won't affect the development on renato-wiki in particular but will definitely be very much appreciated.
