# Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/) and this project adheres to
[Semantic Versioning](http://semver.org/).

## [Unreleased]

## [0.3.0] - 2017-05-28

### Added
- Do not ignore startup errors of plugins to load, throw them instead of fallback error
- Add `{Boolean} production` to /api/
- Reference the index from index data and add a flat index instance

### Changed
- The `parent` property of index data objects now references the parent index data instead of the index itself

## [0.2.0] - 2017-05-15

### Added
- Renato#enableExtension

### Changed
- Improve default configuration for category and page sort
- Rename hook `core::routes` to `core::routes:bind`
- Rename hook `core::index` to `core::index:update:prepare`
- Rename event `core::index:pre` to `core::index:update:pre`
- Rename event `core::index:post` to `core::index:update:post`
- Renato#bindRoutes now returns a bluebird Promise object
- File extension of content pages is now used within hook keys instead of dedicated extension-specific hook maps

### Removed
- Renato#hookReduceIndex
- Renato#getHiddenData

## [0.1.1] - 2017-04-25

### Fixed
- Startup crash

## [0.1.0] - 2017-04-25 [YANKED]

### Added
- Create read-only wiki platform.
