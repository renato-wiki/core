# Contributing to renato-wiki/core

![:handshake:](http://cdn.jsdelivr.net/emojione/assets/png/1f91d.png?v=2.2.4)

First off, thank you for considering a contribution to this project!

Since this is a small project and I have limited time and knowledge, I highly appreciate any help.

## Ways of contribution

There are several ways in which you can help this project.

### Spread the word

The probably most important path during the beginning phase is to help renato-wiki in gaining attention. You can do so
by posting about us within your blog or share a link within developer forums (only if appropriate please). Every
contribution by spreading the word about renato-wiki is important for this project!

### Report issues

If you get to notice any kind of issue within the program or its meta data (e.g. wiki, readme, etc.), please
[share it](https://gitlab.com/renato-wiki/core/issues/new)! Every reported issue or request for
improvement/clarification does contribute towards better code or instructions. Even reports about the language within
instructions would help me to improve those in order to avoid confusion of the users.

An invalid or badly formatted issue report is better than none in almost any cases. Don't hold back on contribution in
this way. But of course I'd appreciate if you try to fulfill some templates as posted below.

Please use the issue search first in order to avoid duplicates.

### Request features

If you're missing any features that seem appropriate to you, please let me know. You can post them within the
[issue tracker](https://gitlab.com/renato-wiki/core/issues/new) or contact me; The contact information is present at the
end of this file.

Please use the issue search first in order to avoid duplicates.

### Merge Requests

If you'd like to participate by code contribution, feel free to send a merge request for the `development` branch.
Consider creating a issue report or feature request beforehand that mentions you're going to work on that one therefor
no other contributors waste their time working on the same issue at the same time. When an issue is already assigned to
someone, you can assume that he/she already had some thoughts about it and would like to do it by him/herself.

## Environment

In order to contribute via merge requests, you need to have the environment set-up correctly for development.

1. Check out the `development` branch. No work shall be done on `master` directly.
2. Ensure you've installed a proper version of node.js (specified within `engines` field of the
   [package.json](https://gitlab.com/renato-wiki/core/blob/development/package.json)) and npm.
3. Run `npm install` within the project root.

### Branches

Always send your merge requests to the `development` branch. Merge requests into the `master` branch will be rejected
with an according comment. The `master` branch is meant to be in sync with the published version on npm.

### Linter

We use [`eslint`](http://eslint.org/) for static code quality enforcements. Check out the
[`.eslintrc`](https://gitlab.com/renato-wiki/core/blob/development/.eslintrc) for the specified set of rules.
You can run the linter with `npm run lint`. Merge requests with errors or warnings during linting won't be accepted
until those are fixed. If warnings are present that would force you to reduce code quality (e.g. in some cases modules
do need a certain amount of lines, etc.), you can disable their inspection on a per file basis; If I do not consider
overrides appropriate, I will state so within a comment.

## Git commits

Any non-merge commits should have a message according to the following rules:

1. Separate subject from body with a blank line (same for body and footer if any)
2. Capitalize the subject line
3. Use the imperative mood in the subject line
4. Do not end the subject line with punctuation
5. Use the body to explain *what* and *why* instead of *how*
6. Keep the subject line short and to the point

Those rules have been derived from [chris.beams.io](http://chris.beams.io/posts/git-commit/).

## Code contribution conditions

By contributing to this project you agree to publish your changes under terms and conditions specified within the
[LICENSE file](LICENSE).

## Recognition of code contribution

If you choose to send a merge request, feel free to add yourself into the `contributors` field of the
[package.json](https://gitlab.com/renato-wiki/core/blob/development/package.json). Please do so in a separate commit
with a message like `Add xyz to contributors`.

## Response and contact

It might take a while until I respond to issues or merge requests. If you think I could've underestimated the
importance (at least a few days no response), feel free to bump with an appropriate comment or notify me via any of the
following contact possibilities:

 * Email: frissdiegurke@protonmail.com

You might as well contact me for any help or questions regarding the application.

## Appendix

Thank you for reading my guide on contribution! I look forward to hearing from you.

*frissdiegurke*
